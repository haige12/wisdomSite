package cn.com.taiji;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

/**
 * 启动程序
 */
@MapperScan("cn.com.taiji.**.mapper")
@EnableScheduling
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class SiteApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(SiteApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }
}
