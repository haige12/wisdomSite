package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.constant.HttpStatus;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.zhgd.domain.ZhgdAttendance;
import cn.com.taiji.zhgd.service.IZhgdAttendanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * 实名考勤-数据同步
 */
@Slf4j
@RestController
@RequestMapping("/real/attendance")
public class ZhgdRealNameAttController {

    @Autowired
    private IZhgdAttendanceService zhgdAttendanceService;

    @PostMapping("/batchPush")
    public AjaxResult add(@Valid @RequestBody List<ZhgdAttendance> zhgdAttendanceList)
    {
        AjaxResult ajaxResult = new AjaxResult(HttpStatus.SUCCESS,"批量推送成功！");
        log.error("----------------------开始批量推送数据---------------------------" + new Date());
        log.error("要接收的推送数据为： " + zhgdAttendanceList.size() + "条");
        try {
            if (zhgdAttendanceList.size()>0){
                zhgdAttendanceList.stream().forEach(item->{
                    item.setCreateTime(new Date());
                    item.setUpdateTime(new Date());
                    item.setRemark("实名考勤第三方设备-数据同步");
                });
                zhgdAttendanceService.batchAdd(zhgdAttendanceList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult = new AjaxResult(HttpStatus.ERROR,"数据保存失败,请联系系统管理员！");
        }
        log.error("----------------------推送数据结束---------------------------" + new Date());
        return ajaxResult;
    }
}
