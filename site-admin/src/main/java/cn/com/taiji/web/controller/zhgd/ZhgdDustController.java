package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.annotation.Log;
import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.common.enums.BusinessType;
import cn.com.taiji.common.utils.poi.ExcelUtil;
import cn.com.taiji.zhgd.domain.ZhgdDust;
import cn.com.taiji.zhgd.service.IZhgdDustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 扬尘管理Controller
 * 
 * @author haige
 * @date 2022-03-22
 */
@RestController
@RequestMapping("/zhgd/zhgd")
public class ZhgdDustController extends BaseController
{
    @Autowired
    private IZhgdDustService zhgdDustService;

    /**
     * 查询扬尘列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZhgdDust zhgdDust)
    {
        startPage();
        List<ZhgdDust> list = zhgdDustService.selectZhgdDustList(zhgdDust);
        return getDataTable(list);
    }

    /**
     * 导出扬尘列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:export')")
    @Log(title = "扬尘", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ZhgdDust zhgdDust)
    {
        List<ZhgdDust> list = zhgdDustService.selectZhgdDustList(zhgdDust);
        ExcelUtil<ZhgdDust> util = new ExcelUtil<ZhgdDust>(ZhgdDust.class);
        return util.exportExcel(list, "扬尘数据");
    }

    /**
     * 获取扬尘详细信息
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zhgdDustService.selectZhgdDustById(id));
    }

    /**
     * 新增扬尘
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:add')")
    @Log(title = "扬尘", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZhgdDust zhgdDust)
    {
        return toAjax(zhgdDustService.insertZhgdDust(zhgdDust));
    }

    /**
     * 修改扬尘
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:edit')")
    @Log(title = "扬尘", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZhgdDust zhgdDust)
    {
        return toAjax(zhgdDustService.updateZhgdDust(zhgdDust));
    }

    /**
     * 删除扬尘
     */
    @PreAuthorize("@ss.hasPermi('zhgd:zhgd:remove')")
    @Log(title = "扬尘", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zhgdDustService.deleteZhgdDustByIds(ids));
    }
}
