package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.annotation.Log;
import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.common.enums.BusinessType;
import cn.com.taiji.common.utils.poi.ExcelUtil;
import cn.com.taiji.zhgd.domain.ZhgdCamera;
import cn.com.taiji.zhgd.service.IZhgdCameraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 摄像头Controller
 * 
 * @author haige
 * @date 2022-03-22
 */
@RestController
@RequestMapping("/zhgd/camera")
public class ZhgdCameraController extends BaseController
{
    @Autowired
    private IZhgdCameraService zhgdCameraService;

    /**
     * 查询摄像头列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZhgdCamera zhgdCamera)
    {
        startPage();
        List<ZhgdCamera> list = zhgdCameraService.selectZhgdCameraList(zhgdCamera);
        return getDataTable(list);
    }

    /**
     * 导出摄像头列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:export')")
    @Log(title = "摄像头", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ZhgdCamera zhgdCamera)
    {
        List<ZhgdCamera> list = zhgdCameraService.selectZhgdCameraList(zhgdCamera);
        ExcelUtil<ZhgdCamera> util = new ExcelUtil<ZhgdCamera>(ZhgdCamera.class);
        return util.exportExcel(list, "摄像头数据");
    }

    /**
     * 获取摄像头详细信息
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zhgdCameraService.selectZhgdCameraById(id));
    }

    /**
     * 新增摄像头
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:add')")
    @Log(title = "摄像头", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZhgdCamera zhgdCamera)
    {
        return toAjax(zhgdCameraService.insertZhgdCamera(zhgdCamera));
    }

    /**
     * 修改摄像头
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:edit')")
    @Log(title = "摄像头", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZhgdCamera zhgdCamera)
    {
        return toAjax(zhgdCameraService.updateZhgdCamera(zhgdCamera));
    }

    /**
     * 删除摄像头
     */
    @PreAuthorize("@ss.hasPermi('zhgd:camera:remove')")
    @Log(title = "摄像头", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zhgdCameraService.deleteZhgdCameraByIds(ids));
    }
}
