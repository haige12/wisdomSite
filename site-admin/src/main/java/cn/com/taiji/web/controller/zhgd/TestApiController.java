package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.zhgd.domain.dto.DeviceQueryDTO;
import cn.com.taiji.zhgd.service.IZhgdOpenCallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/open")
public class TestApiController extends BaseController {

    @Autowired
    private IZhgdOpenCallService zhgdOpenCallService;

    @PostMapping("/api")
    public AjaxResult test(@RequestBody DeviceQueryDTO deviceQueryDTO)
    {
        return zhgdOpenCallService.queryDustDataByDeviceNo(deviceQueryDTO);
    }
}
