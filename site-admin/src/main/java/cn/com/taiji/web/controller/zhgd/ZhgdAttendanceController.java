package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.annotation.Log;
import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.common.enums.BusinessType;
import cn.com.taiji.common.utils.poi.ExcelUtil;
import cn.com.taiji.zhgd.domain.ZhgdAttendance;
import cn.com.taiji.zhgd.service.IZhgdAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 考勤管理Controller
 * 
 * @author haige
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/zhgd/attendance")
public class ZhgdAttendanceController extends BaseController
{
    @Autowired
    private IZhgdAttendanceService zhgdAttendanceService;

    /**
     * 查询考勤管理列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZhgdAttendance zhgdAttendance)
    {
        startPage();
        List<ZhgdAttendance> list = zhgdAttendanceService.selectZhgdAttendanceList(zhgdAttendance);
        return getDataTable(list);
    }

    /**
     * 导出考勤管理列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:export')")
    @Log(title = "考勤管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ZhgdAttendance zhgdAttendance)
    {
        List<ZhgdAttendance> list = zhgdAttendanceService.selectZhgdAttendanceList(zhgdAttendance);
        ExcelUtil<ZhgdAttendance> util = new ExcelUtil<ZhgdAttendance>(ZhgdAttendance.class);
        return util.exportExcel(list, "考勤管理数据");
    }

    /**
     * 获取考勤管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zhgdAttendanceService.selectZhgdAttendanceById(id));
    }

    /**
     * 新增考勤管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:add')")
    @Log(title = "考勤管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZhgdAttendance zhgdAttendance)
    {
        return toAjax(zhgdAttendanceService.insertZhgdAttendance(zhgdAttendance));
    }

    /**
     * 修改考勤管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:edit')")
    @Log(title = "考勤管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZhgdAttendance zhgdAttendance)
    {
        return toAjax(zhgdAttendanceService.updateZhgdAttendance(zhgdAttendance));
    }

    /**
     * 删除考勤管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:attendance:remove')")
    @Log(title = "考勤管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zhgdAttendanceService.deleteZhgdAttendanceByIds(ids));
    }
}
