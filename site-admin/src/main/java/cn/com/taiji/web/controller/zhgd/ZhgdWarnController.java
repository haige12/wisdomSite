package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.annotation.Log;
import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.common.enums.BusinessType;
import cn.com.taiji.common.utils.poi.ExcelUtil;
import cn.com.taiji.zhgd.domain.ZhgdWarn;
import cn.com.taiji.zhgd.service.IZhgdWarnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 预警Controller
 * 
 * @author haige
 * @date 2022-03-22
 */
@RestController
@RequestMapping("/zhgd/warn")
public class ZhgdWarnController extends BaseController
{
    @Autowired
    private IZhgdWarnService zhgdWarnService;

    /**
     * 查询预警列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZhgdWarn zhgdWarn)
    {
        startPage();
        List<ZhgdWarn> list = zhgdWarnService.selectZhgdWarnList(zhgdWarn);
        return getDataTable(list);
    }

    /**
     * 导出预警列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:export')")
    @Log(title = "预警", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ZhgdWarn zhgdWarn)
    {
        List<ZhgdWarn> list = zhgdWarnService.selectZhgdWarnList(zhgdWarn);
        ExcelUtil<ZhgdWarn> util = new ExcelUtil<ZhgdWarn>(ZhgdWarn.class);
        return util.exportExcel(list, "预警数据");
    }

    /**
     * 获取预警详细信息
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zhgdWarnService.selectZhgdWarnById(id));
    }

    /**
     * 新增预警
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:add')")
    @Log(title = "预警", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZhgdWarn zhgdWarn)
    {
        return toAjax(zhgdWarnService.insertZhgdWarn(zhgdWarn));
    }

    /**
     * 修改预警
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:edit')")
    @Log(title = "预警", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZhgdWarn zhgdWarn)
    {
        return toAjax(zhgdWarnService.updateZhgdWarn(zhgdWarn));
    }

    /**
     * 删除预警
     */
    @PreAuthorize("@ss.hasPermi('zhgd:warn:remove')")
    @Log(title = "预警", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zhgdWarnService.deleteZhgdWarnByIds(ids));
    }
}
