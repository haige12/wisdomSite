package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.annotation.Log;
import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.common.enums.BusinessType;
import cn.com.taiji.common.utils.poi.ExcelUtil;
import cn.com.taiji.zhgd.domain.ZhgdCrane;
import cn.com.taiji.zhgd.service.IZhgdCraneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 塔吊管理Controller
 * 
 * @author haige
 * @date 2022-03-22
 */
@RestController
@RequestMapping("/zhgd/crane")
public class ZhgdCraneController extends BaseController
{
    @Autowired
    private IZhgdCraneService zhgdCraneService;

    /**
     * 查询塔吊管理列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZhgdCrane zhgdCrane)
    {
        startPage();
        List<ZhgdCrane> list = zhgdCraneService.selectZhgdCraneList(zhgdCrane);
        return getDataTable(list);
    }

    /**
     * 导出塔吊管理列表
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:export')")
    @Log(title = "塔吊管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ZhgdCrane zhgdCrane)
    {
        List<ZhgdCrane> list = zhgdCraneService.selectZhgdCraneList(zhgdCrane);
        ExcelUtil<ZhgdCrane> util = new ExcelUtil<ZhgdCrane>(ZhgdCrane.class);
        return util.exportExcel(list, "塔吊管理数据");
    }

    /**
     * 获取塔吊管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zhgdCraneService.selectZhgdCraneById(id));
    }

    /**
     * 新增塔吊管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:add')")
    @Log(title = "塔吊管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ZhgdCrane zhgdCrane)
    {
        return toAjax(zhgdCraneService.insertZhgdCrane(zhgdCrane));
    }

    /**
     * 修改塔吊管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:edit')")
    @Log(title = "塔吊管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ZhgdCrane zhgdCrane)
    {
        return toAjax(zhgdCraneService.updateZhgdCrane(zhgdCrane));
    }

    /**
     * 删除塔吊管理
     */
    @PreAuthorize("@ss.hasPermi('zhgd:crane:remove')")
    @Log(title = "塔吊管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zhgdCraneService.deleteZhgdCraneByIds(ids));
    }
}
