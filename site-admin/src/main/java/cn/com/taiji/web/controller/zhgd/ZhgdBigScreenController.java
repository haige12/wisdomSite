package cn.com.taiji.web.controller.zhgd;

import cn.com.taiji.common.core.controller.BaseController;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.core.page.TableDataInfo;
import cn.com.taiji.zhgd.domain.ZhgdWarn;
import cn.com.taiji.zhgd.service.IZhgdBigScreenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 智慧工地-大屏接口
 * @author linhaiy
 * @since 2022.04.23
 */
@RestController
@RequestMapping("/zhgd/screen")
public class ZhgdBigScreenController extends BaseController {

    @Autowired
    private IZhgdBigScreenService zhgdBigScreenService;

    /**
     * 获取最新一条扬尘监测数据
     * @return
     */
    @GetMapping(value = "/getDustInfo")
    public AjaxResult getDustInfo()
    {
        return AjaxResult.success(zhgdBigScreenService.queryDustInfo());
    }

    /**
     * 获取最新一条塔吊监测数据
     * @return
     */
    @GetMapping(value = "/getTowerCraneInfo")
    public AjaxResult getTowerCraneInfo()
    {
        return AjaxResult.success(zhgdBigScreenService.queryTowerCraneInfo());
    }

    /**
     * 获取预警列表
     * @param zhgdWarn
     * @return
     */
    @GetMapping(value = "/getWarnList")
    public TableDataInfo list(ZhgdWarn zhgdWarn)
    {
        startPage();
        List<ZhgdWarn> list = zhgdBigScreenService.queryWarnList(zhgdWarn);
        return getDataTable(list);
    }

    /**
     * 获取预警数据分析-饼图百分比
     * @return
     */
    @GetMapping(value = "/getWarnAnalysis")
    public AjaxResult getWarnAnalysis()
    {
        return AjaxResult.success(zhgdBigScreenService.queryProportion());
    }
}
