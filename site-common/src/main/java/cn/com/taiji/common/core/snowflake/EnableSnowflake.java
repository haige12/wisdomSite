package cn.com.taiji.common.core.snowflake;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 引入雪花算法注解
 * @author linhaiyun
 * @date 2021-08-16
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SnowflakeAutoConfiguration.class)
public @interface EnableSnowflake {
}
