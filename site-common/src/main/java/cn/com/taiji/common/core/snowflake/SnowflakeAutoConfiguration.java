package cn.com.taiji.common.core.snowflake;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 雪花算法配置
 * @author linhaiyun
 * @date 2021-08-16
 */
@Configuration
@EnableConfigurationProperties(SnowflakeProperties.class)
@Slf4j
public class SnowflakeAutoConfiguration {
    @Bean
    @ConditionalOnProperty(prefix = SnowflakeProperties.PREFIX,name = "start")
    public Long start(SnowflakeProperties snowflakeProperties) {
        return snowflakeProperties.getStart();
    }

    @Bean
    @ConditionalOnProperty(prefix = SnowflakeProperties.PREFIX,name = "data")
    public Integer data(SnowflakeProperties snowflakeProperties) {
        return snowflakeProperties.getDatacenterId();
    }


    @Bean
    @ConditionalOnProperty(prefix = SnowflakeProperties.PREFIX,name = "work")
    public Integer work(SnowflakeProperties snowflakeProperties) {
        return snowflakeProperties.getWorkerId();
    }


    @Bean
    public Snowflake snowflake(SnowflakeProperties snowflakeProperties){
        Snowflake snowflake = new Snowflake();
        if (snowflakeProperties.getStart() != null) {
            snowflake.setStartTime(snowflakeProperties.getStart());
        }
        if (snowflakeProperties.getDatacenterId() != null){
            snowflake.setDataIds(snowflakeProperties.getDatacenterId());
        }
        if (snowflakeProperties.getWorkerId() != null){
            snowflake.setWorkIds(snowflakeProperties.getWorkerId());
        }
        log.info("雪花算法starter组件加载完成！开始时间=[{}]", snowflake.getStartTime());
        log.info("雪花算法starter组件加载完成！数据中心_ID=[{}]", snowflake.getDataIds());
        log.info("雪花算法starter组件加载完成！机器_ID=[{}]", snowflake.getWorkIds());
        return snowflake;
    }

/*    public static void main(String[] args) {
        Snowflake snowflake = new Snowflake();
        snowflake.setDATA_ID(3L);
        snowflake.setWORK_ID(2L);
        for (int i=0;i<20;i++){
            System.out.println(snowflake.nextId());
        }
    }*/
}
