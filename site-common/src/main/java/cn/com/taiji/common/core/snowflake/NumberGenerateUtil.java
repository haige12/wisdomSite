package cn.com.taiji.common.core.snowflake;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 86176
 */
@Component
public class NumberGenerateUtil {

    @Autowired
    private Snowflake snowflake;

    public Long getNum() {
        return snowflake.nextId();
    }
}
