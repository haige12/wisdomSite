package cn.com.taiji.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 智慧工地开放服务配置
 * @author linhaiy
 * @since 2022.03.31
 */
@Component
@Data
@ConfigurationProperties(prefix = "wisdom.site")
public class OpenServiceConfig {

    private String keyId;

    private String rcode;

    private String ts;

    private String region;

    private String keySecret;

    private String signature;

    private String url;
}
