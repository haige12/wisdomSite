package cn.com.taiji.common.core.snowflake;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author linhaiyun
 * @date 2021-08-16
 */
@ConfigurationProperties(prefix = "snow.flake")
@Data
public class SnowflakeProperties {
    public static final String PREFIX = "snow.flake";

    /**
     * 开始时间
     */
    private Long start;

    /**
     * 数据中心id
     */
    private Integer datacenterId;

    /**
     * 机器id
     */
    private Integer workerId;

}
