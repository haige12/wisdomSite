import request from '@/utils/request'

// 查询塔吊管理列表
export function listCrane(query) {
  return request({
    url: '/zhgd/crane/list',
    method: 'get',
    params: query
  })
}

// 查询塔吊管理详细
export function getCrane(id) {
  return request({
    url: '/zhgd/crane/' + id,
    method: 'get'
  })
}

// 新增塔吊管理
export function addCrane(data) {
  return request({
    url: '/zhgd/crane',
    method: 'post',
    data: data
  })
}

// 修改塔吊管理
export function updateCrane(data) {
  return request({
    url: '/zhgd/crane',
    method: 'put',
    data: data
  })
}

// 删除塔吊管理
export function delCrane(id) {
  return request({
    url: '/zhgd/crane/' + id,
    method: 'delete'
  })
}

// 导出塔吊管理
export function exportCrane(query) {
  return request({
    url: '/zhgd/crane/export',
    method: 'get',
    params: query
  })
}