import request from '@/utils/request'

// 查询预警列表
export function listWarn(query) {
  return request({
    url: '/zhgd/warn/list',
    method: 'get',
    params: query
  })
}

// 查询预警详细
export function getWarn(id) {
  return request({
    url: '/zhgd/warn/' + id,
    method: 'get'
  })
}

// 新增预警
export function addWarn(data) {
  return request({
    url: '/zhgd/warn',
    method: 'post',
    data: data
  })
}

// 修改预警
export function updateWarn(data) {
  return request({
    url: '/zhgd/warn',
    method: 'put',
    data: data
  })
}

// 删除预警
export function delWarn(id) {
  return request({
    url: '/zhgd/warn/' + id,
    method: 'delete'
  })
}

// 导出预警
export function exportWarn(query) {
  return request({
    url: '/zhgd/warn/export',
    method: 'get',
    params: query
  })
}