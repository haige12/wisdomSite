import request from '@/utils/request'

// 查询扬尘列表
export function listZhgd(query) {
  return request({
    url: '/zhgd/zhgd/list',
    method: 'get',
    params: query
  })
}

// 查询扬尘详细
export function getZhgd(id) {
  return request({
    url: '/zhgd/zhgd/' + id,
    method: 'get'
  })
}

// 新增扬尘
export function addZhgd(data) {
  return request({
    url: '/zhgd/zhgd',
    method: 'post',
    data: data
  })
}

// 修改扬尘
export function updateZhgd(data) {
  return request({
    url: '/zhgd/zhgd',
    method: 'put',
    data: data
  })
}

// 删除扬尘
export function delZhgd(id) {
  return request({
    url: '/zhgd/zhgd/' + id,
    method: 'delete'
  })
}

// 导出扬尘
export function exportZhgd(query) {
  return request({
    url: '/zhgd/zhgd/export',
    method: 'get',
    params: query
  })
}