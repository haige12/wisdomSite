package cn.com.taiji.zhgd.service.impl;

import java.util.List;

import cn.com.taiji.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.taiji.zhgd.mapper.ZhgdTowerCraneMapper;
import cn.com.taiji.zhgd.domain.ZhgdTowerCrane;
import cn.com.taiji.zhgd.service.IZhgdTowerCraneService;

/**
 * 塔吊设备-同步开放平台数据Service业务层处理
 * 
 * @author haige
 * @date 2022-04-23
 */
@Service
public class ZhgdTowerCraneServiceImpl implements IZhgdTowerCraneService 
{
    @Autowired
    private ZhgdTowerCraneMapper zhgdTowerCraneMapper;

    /**
     * 查询塔吊设备-同步开放平台数据
     * 
     * @param id 塔吊设备-同步开放平台数据主键
     * @return 塔吊设备-同步开放平台数据
     */
    @Override
    public ZhgdTowerCrane selectZhgdTowerCraneById(Long id)
    {
        return zhgdTowerCraneMapper.selectZhgdTowerCraneById(id);
    }

    /**
     * 查询塔吊设备-同步开放平台数据列表
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 塔吊设备-同步开放平台数据
     */
    @Override
    public List<ZhgdTowerCrane> selectZhgdTowerCraneList(ZhgdTowerCrane zhgdTowerCrane)
    {
        return zhgdTowerCraneMapper.selectZhgdTowerCraneList(zhgdTowerCrane);
    }

    /**
     * 新增塔吊设备-同步开放平台数据
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 结果
     */
    @Override
    public int insertZhgdTowerCrane(ZhgdTowerCrane zhgdTowerCrane)
    {
        zhgdTowerCrane.setCreateTime(DateUtils.getNowDate());
        return zhgdTowerCraneMapper.insertZhgdTowerCrane(zhgdTowerCrane);
    }

    /**
     * 修改塔吊设备-同步开放平台数据
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 结果
     */
    @Override
    public int updateZhgdTowerCrane(ZhgdTowerCrane zhgdTowerCrane)
    {
        zhgdTowerCrane.setUpdateTime(DateUtils.getNowDate());
        return zhgdTowerCraneMapper.updateZhgdTowerCrane(zhgdTowerCrane);
    }

    /**
     * 批量删除塔吊设备-同步开放平台数据
     * 
     * @param ids 需要删除的塔吊设备-同步开放平台数据主键
     * @return 结果
     */
    @Override
    public int deleteZhgdTowerCraneByIds(Long[] ids)
    {
        return zhgdTowerCraneMapper.deleteZhgdTowerCraneByIds(ids);
    }

    /**
     * 删除塔吊设备-同步开放平台数据信息
     * 
     * @param id 塔吊设备-同步开放平台数据主键
     * @return 结果
     */
    @Override
    public int deleteZhgdTowerCraneById(Long id)
    {
        return zhgdTowerCraneMapper.deleteZhgdTowerCraneById(id);
    }
}
