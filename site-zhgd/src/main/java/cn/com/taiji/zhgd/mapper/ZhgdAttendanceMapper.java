package cn.com.taiji.zhgd.mapper;

import java.util.List;
import cn.com.taiji.zhgd.domain.ZhgdAttendance;
import org.apache.ibatis.annotations.Param;

/**
 * 考勤管理Mapper接口
 * 
 * @author haige
 * @date 2022-03-30
 */
public interface ZhgdAttendanceMapper 
{
    /**
     * 查询考勤管理
     * 
     * @param id 考勤管理主键
     * @return 考勤管理
     */
    public ZhgdAttendance selectZhgdAttendanceById(Long id);

    /**
     * 查询考勤管理列表
     * 
     * @param zhgdAttendance 考勤管理
     * @return 考勤管理集合
     */
    public List<ZhgdAttendance> selectZhgdAttendanceList(ZhgdAttendance zhgdAttendance);

    /**
     * 新增考勤管理
     * 
     * @param zhgdAttendance 考勤管理
     * @return 结果
     */
    public int insertZhgdAttendance(ZhgdAttendance zhgdAttendance);

    /**
     * 修改考勤管理
     * 
     * @param zhgdAttendance 考勤管理
     * @return 结果
     */
    public int updateZhgdAttendance(ZhgdAttendance zhgdAttendance);

    /**
     * 删除考勤管理
     * 
     * @param id 考勤管理主键
     * @return 结果
     */
    public int deleteZhgdAttendanceById(Long id);

    /**
     * 批量删除考勤管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhgdAttendanceByIds(Long[] ids);

    /**
     * 批量添加考勤数据
     * @param zhgdAttendanceList
     * @return
     */
    int batchSave(@Param(value = "zhgdAttendanceList") List<ZhgdAttendance> zhgdAttendanceList);
}
