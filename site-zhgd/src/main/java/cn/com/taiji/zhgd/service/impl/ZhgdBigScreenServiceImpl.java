package cn.com.taiji.zhgd.service.impl;

import cn.com.taiji.zhgd.domain.ZhgdDust;
import cn.com.taiji.zhgd.domain.ZhgdTowerCrane;
import cn.com.taiji.zhgd.domain.ZhgdWarn;
import cn.com.taiji.zhgd.domain.dto.WarnPercentTjDTO;
import cn.com.taiji.zhgd.mapper.ZhgdDustMapper;
import cn.com.taiji.zhgd.mapper.ZhgdTowerCraneMapper;
import cn.com.taiji.zhgd.mapper.ZhgdWarnMapper;
import cn.com.taiji.zhgd.service.IZhgdBigScreenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ZhgdBigScreenServiceImpl implements IZhgdBigScreenService {

    @Autowired
    private ZhgdDustMapper zhgdDustMapper;

    @Autowired
    private ZhgdTowerCraneMapper zhgdTowerCraneMapper;

    @Autowired
    private ZhgdWarnMapper zhgdWarnMapper;

    @Override
    public ZhgdDust queryDustInfo() {
        return zhgdDustMapper.selectZhgdDustByOne();
    }

    @Override
    public ZhgdTowerCrane queryTowerCraneInfo(){
        return zhgdTowerCraneMapper.selectZhgdTowerCraneByOne();
    }

    @Override
    public List<ZhgdWarn> queryWarnList(ZhgdWarn zhgdWarn){
        return zhgdWarnMapper.selectZhgdWarnList(zhgdWarn);
    }

    @Override
    public List<WarnPercentTjDTO> queryProportion(){
        List<WarnPercentTjDTO> warnPercentTjDTOList = new ArrayList<>();
        warnPercentTjDTOList.add(new WarnPercentTjDTO("抽烟","50.00%"));
        warnPercentTjDTOList.add(new WarnPercentTjDTO("登高","25.00%"));
        warnPercentTjDTOList.add(new WarnPercentTjDTO("脱戴","25.00%"));
        warnPercentTjDTOList.add(new WarnPercentTjDTO("反光衣","0.00%"));
        warnPercentTjDTOList.add(new WarnPercentTjDTO("其他","0.00%"));
        return warnPercentTjDTOList;
    }
}
