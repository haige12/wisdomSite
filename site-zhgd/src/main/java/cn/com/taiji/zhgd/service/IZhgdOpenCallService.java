package cn.com.taiji.zhgd.service;

import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.zhgd.domain.dto.DeviceQueryDTO;

/**
 * 开发服务调用
 * @author linhaiy
 * @since 2022.04.01
 */
public interface IZhgdOpenCallService {

    AjaxResult queryDustDataByDeviceNo(DeviceQueryDTO deviceQueryDTO);
}
