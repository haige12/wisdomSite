package cn.com.taiji.zhgd.mapper;

import java.util.List;
import cn.com.taiji.zhgd.domain.ZhgdWarn;

/**
 * 预警Mapper接口
 * 
 * @author haige
 * @date 2022-03-22
 */
public interface ZhgdWarnMapper 
{
    /**
     * 查询预警
     * 
     * @param id 预警主键
     * @return 预警
     */
    public ZhgdWarn selectZhgdWarnById(Long id);

    /**
     * 查询预警列表
     * 
     * @param zhgdWarn 预警
     * @return 预警集合
     */
    public List<ZhgdWarn> selectZhgdWarnList(ZhgdWarn zhgdWarn);

    /**
     * 新增预警
     * 
     * @param zhgdWarn 预警
     * @return 结果
     */
    public int insertZhgdWarn(ZhgdWarn zhgdWarn);

    /**
     * 修改预警
     * 
     * @param zhgdWarn 预警
     * @return 结果
     */
    public int updateZhgdWarn(ZhgdWarn zhgdWarn);

    /**
     * 删除预警
     * 
     * @param id 预警主键
     * @return 结果
     */
    public int deleteZhgdWarnById(Long id);

    /**
     * 批量删除预警
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhgdWarnByIds(Long[] ids);
}
