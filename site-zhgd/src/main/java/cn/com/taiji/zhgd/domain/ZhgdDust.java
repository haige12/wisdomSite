package cn.com.taiji.zhgd.domain;

import cn.com.taiji.common.annotation.Excel;
import cn.com.taiji.common.core.domain.BaseEntity;
import lombok.Builder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 扬尘对象 zhgd_dust
 * 
 * @author haige
 * @date 2022-03-22
 */
public class ZhgdDust extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** PM2.5 */
    @Excel(name = "PM2.5")
    private String pmTwo;

    /** PM10 */
    @Excel(name = "PM10")
    private String pmTen;

    /** 风速 */
    @Excel(name = "风速")
    private String windSpeed;

    /** 风向 */
    @Excel(name = "风向")
    private String windDirection;

    /** 风力 */
    @Excel(name = "风力")
    private String windPower;

    /** 噪音 */
    @Excel(name = "噪音")
    private String noise;

    /** TSP */
    @Excel(name = "TSP")
    private String tsp;

    /** 温度 */
    @Excel(name = "温度")
    private String temperature;

    /** 湿度 */
    @Excel(name = "湿度")
    private String humidity;

    /** 气压 */
    @Excel(name = "气压")
    private String airPressure;

    /** 状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPmTwo(String pmTwo) 
    {
        this.pmTwo = pmTwo;
    }

    public String getPmTwo() 
    {
        return pmTwo;
    }
    public void setPmTen(String pmTen) 
    {
        this.pmTen = pmTen;
    }

    public String getPmTen() 
    {
        return pmTen;
    }
    public void setWindSpeed(String windSpeed) 
    {
        this.windSpeed = windSpeed;
    }

    public String getWindSpeed() 
    {
        return windSpeed;
    }
    public void setWindDirection(String windDirection) 
    {
        this.windDirection = windDirection;
    }

    public String getWindDirection() 
    {
        return windDirection;
    }
    public void setWindPower(String windPower) 
    {
        this.windPower = windPower;
    }

    public String getWindPower() 
    {
        return windPower;
    }
    public void setNoise(String noise) 
    {
        this.noise = noise;
    }

    public String getNoise() 
    {
        return noise;
    }
    public void setTsp(String tsp) 
    {
        this.tsp = tsp;
    }

    public String getTsp() 
    {
        return tsp;
    }
    public void setTemperature(String temperature) 
    {
        this.temperature = temperature;
    }

    public String getTemperature() 
    {
        return temperature;
    }
    public void setHumidity(String humidity) 
    {
        this.humidity = humidity;
    }

    public String getHumidity() 
    {
        return humidity;
    }
    public void setAirPressure(String airPressure) 
    {
        this.airPressure = airPressure;
    }

    public String getAirPressure() 
    {
        return airPressure;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pmTwo", getPmTwo())
            .append("pmTen", getPmTen())
            .append("windSpeed", getWindSpeed())
            .append("windDirection", getWindDirection())
            .append("windPower", getWindPower())
            .append("noise", getNoise())
            .append("tsp", getTsp())
            .append("temperature", getTemperature())
            .append("humidity", getHumidity())
            .append("airPressure", getAirPressure())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
