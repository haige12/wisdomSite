package cn.com.taiji.zhgd.domain;

import java.math.BigDecimal;

import cn.com.taiji.common.annotation.Excel;
import cn.com.taiji.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 塔吊设备 zhgd_tower_crane
 * 
 * @author haige
 * @date 2022-04-23
 */
public class ZhgdTowerCrane extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private String device;

    /** 幅度 */
    @Excel(name = "幅度")
    private Long amplitude;

    /** 上报时间 */
    @Excel(name = "上报时间")
    private String dateTime;

    /** 倾角 */
    @Excel(name = "倾角")
    private BigDecimal dip;

    /** 塔机高度 */
    @Excel(name = "塔机高度")
    private BigDecimal height;

    /** 设备型号 */
    @Excel(name = "设备型号")
    private String model;

    /** 倍率 */
    @Excel(name = "倍率")
    private Long multiplies;

    /** 噪音 */
    @Excel(name = "噪音")
    private BigDecimal noise;

    /** PM2.5 */
    @Excel(name = "PM2.5")
    private BigDecimal pm25;

    /** 回转角度 */
    @Excel(name = "回转角度")
    private Long rotation;

    /** 回转标志(0左 1右) */
    @Excel(name = "回转标志(0左 1右)")
    private Integer rotationFlag;

    /** 额定重量 */
    @Excel(name = "额定重量")
    private BigDecimal specifiedWeight;

    /** 力矩百分比 */
    @Excel(name = "力矩百分比")
    private Integer torquePercent;

    /** 是否报警(0 正常 1 报警) */
    @Excel(name = "是否报警(0 正常 1 报警)")
    private Integer warn;

    /** 重量 */
    @Excel(name = "重量")
    private BigDecimal weight;

    /** 风速 */
    @Excel(name = "风速")
    private BigDecimal windSpeed;

    /** 预留数据 */
    private String creserve;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDevice(String device) 
    {
        this.device = device;
    }

    public String getDevice() 
    {
        return device;
    }
    public void setAmplitude(Long amplitude) 
    {
        this.amplitude = amplitude;
    }

    public Long getAmplitude() 
    {
        return amplitude;
    }
    public void setDateTime(String dateTime) 
    {
        this.dateTime = dateTime;
    }

    public String getDateTime() 
    {
        return dateTime;
    }
    public void setDip(BigDecimal dip) 
    {
        this.dip = dip;
    }

    public BigDecimal getDip() 
    {
        return dip;
    }
    public void setHeight(BigDecimal height) 
    {
        this.height = height;
    }

    public BigDecimal getHeight() 
    {
        return height;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setMultiplies(Long multiplies) 
    {
        this.multiplies = multiplies;
    }

    public Long getMultiplies() 
    {
        return multiplies;
    }
    public void setNoise(BigDecimal noise) 
    {
        this.noise = noise;
    }

    public BigDecimal getNoise() 
    {
        return noise;
    }
    public void setPm25(BigDecimal pm25) 
    {
        this.pm25 = pm25;
    }

    public BigDecimal getPm25() 
    {
        return pm25;
    }
    public void setRotation(Long rotation) 
    {
        this.rotation = rotation;
    }

    public Long getRotation() 
    {
        return rotation;
    }
    public void setRotationFlag(Integer rotationFlag) 
    {
        this.rotationFlag = rotationFlag;
    }

    public Integer getRotationFlag() 
    {
        return rotationFlag;
    }
    public void setSpecifiedWeight(BigDecimal specifiedWeight) 
    {
        this.specifiedWeight = specifiedWeight;
    }

    public BigDecimal getSpecifiedWeight() 
    {
        return specifiedWeight;
    }
    public void setTorquePercent(Integer torquePercent) 
    {
        this.torquePercent = torquePercent;
    }

    public Integer getTorquePercent() 
    {
        return torquePercent;
    }
    public void setWarn(Integer warn) 
    {
        this.warn = warn;
    }

    public Integer getWarn() 
    {
        return warn;
    }
    public void setWeight(BigDecimal weight) 
    {
        this.weight = weight;
    }

    public BigDecimal getWeight() 
    {
        return weight;
    }
    public void setWindSpeed(BigDecimal windSpeed) 
    {
        this.windSpeed = windSpeed;
    }

    public BigDecimal getWindSpeed() 
    {
        return windSpeed;
    }
    public void setCreserve(String creserve) 
    {
        this.creserve = creserve;
    }

    public String getCreserve() 
    {
        return creserve;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("device", getDevice())
            .append("amplitude", getAmplitude())
            .append("dateTime", getDateTime())
            .append("dip", getDip())
            .append("height", getHeight())
            .append("model", getModel())
            .append("multiplies", getMultiplies())
            .append("noise", getNoise())
            .append("pm25", getPm25())
            .append("rotation", getRotation())
            .append("rotationFlag", getRotationFlag())
            .append("specifiedWeight", getSpecifiedWeight())
            .append("torquePercent", getTorquePercent())
            .append("warn", getWarn())
            .append("weight", getWeight())
            .append("windSpeed", getWindSpeed())
            .append("creserve", getCreserve())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
