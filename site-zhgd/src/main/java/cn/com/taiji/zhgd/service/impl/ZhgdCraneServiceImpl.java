package cn.com.taiji.zhgd.service.impl;

import java.util.List;

import cn.com.taiji.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.taiji.zhgd.mapper.ZhgdCraneMapper;
import cn.com.taiji.zhgd.domain.ZhgdCrane;
import cn.com.taiji.zhgd.service.IZhgdCraneService;

/**
 * 塔吊管理Service业务层处理
 * 
 * @author haige
 * @date 2022-03-22
 */
@Service
public class ZhgdCraneServiceImpl implements IZhgdCraneService 
{
    @Autowired
    private ZhgdCraneMapper zhgdCraneMapper;

    /**
     * 查询塔吊管理
     * 
     * @param id 塔吊管理主键
     * @return 塔吊管理
     */
    @Override
    public ZhgdCrane selectZhgdCraneById(Long id)
    {
        return zhgdCraneMapper.selectZhgdCraneById(id);
    }

    /**
     * 查询塔吊管理列表
     * 
     * @param zhgdCrane 塔吊管理
     * @return 塔吊管理
     */
    @Override
    public List<ZhgdCrane> selectZhgdCraneList(ZhgdCrane zhgdCrane)
    {
        return zhgdCraneMapper.selectZhgdCraneList(zhgdCrane);
    }

    /**
     * 新增塔吊管理
     * 
     * @param zhgdCrane 塔吊管理
     * @return 结果
     */
    @Override
    public int insertZhgdCrane(ZhgdCrane zhgdCrane)
    {
        zhgdCrane.setCreateTime(DateUtils.getNowDate());
        return zhgdCraneMapper.insertZhgdCrane(zhgdCrane);
    }

    /**
     * 修改塔吊管理
     * 
     * @param zhgdCrane 塔吊管理
     * @return 结果
     */
    @Override
    public int updateZhgdCrane(ZhgdCrane zhgdCrane)
    {
        zhgdCrane.setUpdateTime(DateUtils.getNowDate());
        return zhgdCraneMapper.updateZhgdCrane(zhgdCrane);
    }

    /**
     * 批量删除塔吊管理
     * 
     * @param ids 需要删除的塔吊管理主键
     * @return 结果
     */
    @Override
    public int deleteZhgdCraneByIds(Long[] ids)
    {
        return zhgdCraneMapper.deleteZhgdCraneByIds(ids);
    }

    /**
     * 删除塔吊管理信息
     * 
     * @param id 塔吊管理主键
     * @return 结果
     */
    @Override
    public int deleteZhgdCraneById(Long id)
    {
        return zhgdCraneMapper.deleteZhgdCraneById(id);
    }
}
