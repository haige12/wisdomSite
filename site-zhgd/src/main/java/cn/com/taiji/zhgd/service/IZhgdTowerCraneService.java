package cn.com.taiji.zhgd.service;

import java.util.List;
import cn.com.taiji.zhgd.domain.ZhgdTowerCrane;

/**
 * 塔吊设备-同步开放平台数据Service接口
 * 
 * @author haige
 * @date 2022-04-23
 */
public interface IZhgdTowerCraneService 
{
    /**
     * 查询塔吊设备-同步开放平台数据
     * 
     * @param id 塔吊设备-同步开放平台数据主键
     * @return 塔吊设备-同步开放平台数据
     */
    public ZhgdTowerCrane selectZhgdTowerCraneById(Long id);

    /**
     * 查询塔吊设备-同步开放平台数据列表
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 塔吊设备-同步开放平台数据集合
     */
    public List<ZhgdTowerCrane> selectZhgdTowerCraneList(ZhgdTowerCrane zhgdTowerCrane);

    /**
     * 新增塔吊设备-同步开放平台数据
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 结果
     */
    public int insertZhgdTowerCrane(ZhgdTowerCrane zhgdTowerCrane);

    /**
     * 修改塔吊设备-同步开放平台数据
     * 
     * @param zhgdTowerCrane 塔吊设备-同步开放平台数据
     * @return 结果
     */
    public int updateZhgdTowerCrane(ZhgdTowerCrane zhgdTowerCrane);

    /**
     * 批量删除塔吊设备-同步开放平台数据
     * 
     * @param ids 需要删除的塔吊设备-同步开放平台数据主键集合
     * @return 结果
     */
    public int deleteZhgdTowerCraneByIds(Long[] ids);

    /**
     * 删除塔吊设备-同步开放平台数据信息
     * 
     * @param id 塔吊设备-同步开放平台数据主键
     * @return 结果
     */
    public int deleteZhgdTowerCraneById(Long id);
}
