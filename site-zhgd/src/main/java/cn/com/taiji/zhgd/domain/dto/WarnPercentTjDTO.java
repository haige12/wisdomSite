package cn.com.taiji.zhgd.domain.dto;

import lombok.Data;

/**
 * 预警统计百分比
 * @author linhaiy
 * @since 2022.04.23
 */
@Data
public class WarnPercentTjDTO {

    /** 预警类型*/
    private String warnType;

    /** 占比-百分比 */
    private String percent;

    public WarnPercentTjDTO(String warnType, String percent) {
        this.warnType = warnType;
        this.percent = percent;
    }
}
