package cn.com.taiji.zhgd.service.impl;

import cn.com.taiji.common.config.OpenServiceConfig;
import cn.com.taiji.common.constant.HttpStatus;
import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.common.utils.http.HttpCallUtils;
import cn.com.taiji.zhgd.domain.dto.DeviceQueryDTO;
import cn.com.taiji.zhgd.domain.dto.EnvMonitorDTO;
import cn.com.taiji.zhgd.service.IZhgdOpenCallService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class ZhgdOpenCallServiceImpl implements IZhgdOpenCallService {

    @Autowired
    private HttpCallUtils httpCallUtils;

    @Resource
    private OpenServiceConfig openServiceConfig;

    @Override
    public AjaxResult queryDustDataByDeviceNo(DeviceQueryDTO deviceQueryDTO) {
        AjaxResult ajaxResult = new AjaxResult();
        EnvMonitorDTO env = new EnvMonitorDTO();
        String url = openServiceConfig.getUrl() + "/data/live";
        String result = httpCallUtils.sendJsonPost(url, JSON.toJSONString(deviceQueryDTO));
        JSONObject json = JSONObject.parseObject(result);
        String code = json.get("code").toString();
        if ("0".equals(code)){
            env = JSONObject.parseObject(JSON.toJSONString(json.get("data")),EnvMonitorDTO.class);
            env.setDevice(json.get("device_no").toString());
            ajaxResult = new AjaxResult(HttpStatus.SUCCESS,"成功",env);
        }else {
            ajaxResult = new AjaxResult(HttpStatus.NO_CONTENT,"无数据");
        }
        return ajaxResult;
    }
}
