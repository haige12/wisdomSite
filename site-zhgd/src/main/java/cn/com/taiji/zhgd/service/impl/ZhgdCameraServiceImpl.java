package cn.com.taiji.zhgd.service.impl;

import cn.com.taiji.common.utils.DateUtils;
import cn.com.taiji.zhgd.domain.ZhgdCamera;
import cn.com.taiji.zhgd.mapper.ZhgdCameraMapper;
import cn.com.taiji.zhgd.service.IZhgdCameraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 摄像头Service业务层处理
 * 
 * @author haige
 * @date 2022-03-22
 */
@Service
public class ZhgdCameraServiceImpl implements IZhgdCameraService
{
    @Autowired
    private ZhgdCameraMapper zhgdCameraMapper;

    /**
     * 查询摄像头
     * 
     * @param id 摄像头主键
     * @return 摄像头
     */
    @Override
    public ZhgdCamera selectZhgdCameraById(Long id)
    {
        return zhgdCameraMapper.selectZhgdCameraById(id);
    }

    /**
     * 查询摄像头列表
     * 
     * @param zhgdCamera 摄像头
     * @return 摄像头
     */
    @Override
    public List<ZhgdCamera> selectZhgdCameraList(ZhgdCamera zhgdCamera)
    {
        return zhgdCameraMapper.selectZhgdCameraList(zhgdCamera);
    }

    /**
     * 新增摄像头
     * 
     * @param zhgdCamera 摄像头
     * @return 结果
     */
    @Override
    public int insertZhgdCamera(ZhgdCamera zhgdCamera)
    {
        zhgdCamera.setCreateTime(DateUtils.getNowDate());
        return zhgdCameraMapper.insertZhgdCamera(zhgdCamera);
    }

    /**
     * 修改摄像头
     * 
     * @param zhgdCamera 摄像头
     * @return 结果
     */
    @Override
    public int updateZhgdCamera(ZhgdCamera zhgdCamera)
    {
        zhgdCamera.setUpdateTime(DateUtils.getNowDate());
        return zhgdCameraMapper.updateZhgdCamera(zhgdCamera);
    }

    /**
     * 批量删除摄像头
     * 
     * @param ids 需要删除的摄像头主键
     * @return 结果
     */
    @Override
    public int deleteZhgdCameraByIds(Long[] ids)
    {
        return zhgdCameraMapper.deleteZhgdCameraByIds(ids);
    }

    /**
     * 删除摄像头信息
     * 
     * @param id 摄像头主键
     * @return 结果
     */
    @Override
    public int deleteZhgdCameraById(Long id)
    {
        return zhgdCameraMapper.deleteZhgdCameraById(id);
    }
}
