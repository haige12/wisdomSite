package cn.com.taiji.zhgd.task;

import cn.com.taiji.common.core.domain.AjaxResult;
import cn.com.taiji.zhgd.domain.ZhgdDust;
import cn.com.taiji.zhgd.domain.dto.DeviceQueryDTO;
import cn.com.taiji.zhgd.domain.dto.EnvMonitorDTO;
import cn.com.taiji.zhgd.mapper.ZhgdDustMapper;
import cn.com.taiji.zhgd.service.IZhgdOpenCallService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 开放服务同步数据
 * @author linhaiy
 * @since 2022.04.01
 */
@Slf4j
@Component
public class SynDataTask {

    @Autowired
    private IZhgdOpenCallService zhgdOpenCallService;

    @Autowired
    private ZhgdDustMapper zhgdDustMapper;

    /**
     * 每小时同步一次环境监测数据
     */
    @Scheduled(fixedDelay = 3600 * 1000)
    public void executeTask(){
        log.error("Environmental monitoring data synchronization ： " + new Date());
        AjaxResult ajaxResult = zhgdOpenCallService.queryDustDataByDeviceNo(DeviceQueryDTO.builder().device_no("40146268").device_type("env").build());
        String code = ajaxResult.get("code").toString();
        if ("200".equals(code)){
            EnvMonitorDTO env = JSONObject.parseObject(JSON.toJSONString(ajaxResult.get("data")),EnvMonitorDTO.class);
            log.error("环境监测服务返回数据： " + JSON.toJSONString(env));
            //更新扬尘数据
//            zhgdDustMapper.truncateTable();
            ZhgdDust zhgdDust = new ZhgdDust();
            zhgdDust.setPmTwo(env.getPM25().toString());
            zhgdDust.setPmTen(env.getPM10().toString());
            zhgdDust.setWindSpeed(env.getWind().toString());
            zhgdDust.setWindDirection(env.getWindFlag());
            zhgdDust.setWindPower(env.getWindFlagVal().toString());
            zhgdDust.setNoise(env.getNoise().toString());
            zhgdDust.setTsp(env.getTSP().toString());
            zhgdDust.setTemperature(env.getTemperature().toString());
            zhgdDust.setHumidity(env.getHumidity().toString());
            zhgdDust.setCreateBy("智慧工地第三方设备-数据同步");
            zhgdDust.setCreateTime(new Date());
            zhgdDustMapper.insertZhgdDust(zhgdDust);
        }
    }
}
