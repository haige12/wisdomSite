package cn.com.taiji.zhgd.domain.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceQueryDTO {

    /** 设备卡号/编号 */
    private String device_no;

    /** 设备类型 */
    private String device_type;
}
