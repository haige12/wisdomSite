package cn.com.taiji.zhgd.domain;

import cn.com.taiji.common.annotation.Excel;
import cn.com.taiji.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 预警对象 zhgd_warn
 * 
 * @author haige
 * @date 2022-03-22
 */
public class ZhgdWarn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 预警类型 */
    @Excel(name = "预警类型")
    private String warnType;

    /** 摄像头id */
    @Excel(name = "摄像头id")
    private Long cameraId;

    /** 预警摄像头名称，冗余字段 */
    @Excel(name = "预警摄像头名称，冗余字段")
    private String cameraName;

    /** 预警时间 */
    @Excel(name = "预警时间")
    private String warnTime;

    /** 预警信息 */
    @Excel(name = "预警信息")
    private String warnInfo;

    /** 状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWarnType(String warnType) 
    {
        this.warnType = warnType;
    }

    public String getWarnType() 
    {
        return warnType;
    }
    public void setCameraId(Long cameraId) 
    {
        this.cameraId = cameraId;
    }

    public Long getCameraId() 
    {
        return cameraId;
    }
    public void setCameraName(String cameraName) 
    {
        this.cameraName = cameraName;
    }

    public String getCameraName() 
    {
        return cameraName;
    }
    public void setWarnTime(String warnTime) 
    {
        this.warnTime = warnTime;
    }

    public String getWarnTime() 
    {
        return warnTime;
    }
    public void setWarnInfo(String warnInfo) 
    {
        this.warnInfo = warnInfo;
    }

    public String getWarnInfo() 
    {
        return warnInfo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("warnType", getWarnType())
            .append("cameraId", getCameraId())
            .append("cameraName", getCameraName())
            .append("warnTime", getWarnTime())
            .append("warnInfo", getWarnInfo())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
