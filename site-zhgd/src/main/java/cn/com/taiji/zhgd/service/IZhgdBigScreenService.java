package cn.com.taiji.zhgd.service;

import cn.com.taiji.zhgd.domain.ZhgdDust;
import cn.com.taiji.zhgd.domain.ZhgdTowerCrane;
import cn.com.taiji.zhgd.domain.ZhgdWarn;
import cn.com.taiji.zhgd.domain.dto.WarnPercentTjDTO;

import java.util.List;

/**
 * 大屏服务
 */
public interface IZhgdBigScreenService {

    /**
     * 查询最新一条扬尘监测信息
     * @return
     */
    ZhgdDust queryDustInfo();

    /**
     * 查询最新一条塔吊监测信息
     * @return
     */
    ZhgdTowerCrane queryTowerCraneInfo();

    /**
     * 获取预警列表
     * @param zhgdWarn
     * @return
     */
    List<ZhgdWarn> queryWarnList(ZhgdWarn zhgdWarn);

    /**
     * 获取每一项预警类型的占比
     * @return
     */
    List<WarnPercentTjDTO> queryProportion();
}
