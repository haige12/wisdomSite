package cn.com.taiji.zhgd.mapper;

import java.util.List;
import cn.com.taiji.zhgd.domain.ZhgdDust;

/**
 * 扬尘Mapper接口
 * 
 * @author haige
 * @date 2022-03-22
 */
public interface ZhgdDustMapper 
{
    /**
     * 查询扬尘
     * 
     * @param id 扬尘主键
     * @return 扬尘
     */
    public ZhgdDust selectZhgdDustById(Long id);

    /**
     * 查询扬尘列表
     * 
     * @param zhgdDust 扬尘
     * @return 扬尘集合
     */
    public List<ZhgdDust> selectZhgdDustList(ZhgdDust zhgdDust);

    /**
     * 新增扬尘
     * 
     * @param zhgdDust 扬尘
     * @return 结果
     */
    public int insertZhgdDust(ZhgdDust zhgdDust);

    /**
     * 修改扬尘
     * 
     * @param zhgdDust 扬尘
     * @return 结果
     */
    public int updateZhgdDust(ZhgdDust zhgdDust);

    /**
     * 删除扬尘
     * 
     * @param id 扬尘主键
     * @return 结果
     */
    public int deleteZhgdDustById(Long id);

    /**
     * 批量删除扬尘
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhgdDustByIds(Long[] ids);

    /**
     * 清空表数据
     * @return
     */
    int truncateTable();

    /**
     * 查询最新的一条扬尘监测信息
     * @return
     */
    ZhgdDust selectZhgdDustByOne();
}
