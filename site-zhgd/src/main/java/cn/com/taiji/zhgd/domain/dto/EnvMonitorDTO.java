package cn.com.taiji.zhgd.domain.dto;

import lombok.Data;

/**
 * 环境监测返回数据
 */
@Data
public class EnvMonitorDTO {

    /** 设备编码 */
    private String Device;

    /** Pm2.5 */
    private Double PM25;

    /** PM10 */
    private Double PM10;

    /** 温度 */
    private Double Temperature;

    /** 湿度 */
    private Double Humidity;

    /** 噪音 */
    private Double Noise;

    /** 风速 */
    private Double Wind;

    /** 风向(东北) */
    private String WindFlag;

    /** 风向原始值 */
    private Integer WindFlagVal;

    /** 上报时间 */
    private String DateTime;

    /** 是否报警(0 正常 1 报警) */
    private Integer Warn;

    private Integer TSP;
}
