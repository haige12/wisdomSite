package cn.com.taiji.zhgd.service.impl;

import java.util.List;

import cn.com.taiji.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.taiji.zhgd.mapper.ZhgdWarnMapper;
import cn.com.taiji.zhgd.domain.ZhgdWarn;
import cn.com.taiji.zhgd.service.IZhgdWarnService;

/**
 * 预警Service业务层处理
 * 
 * @author haige
 * @date 2022-03-22
 */
@Service
public class ZhgdWarnServiceImpl implements IZhgdWarnService 
{
    @Autowired
    private ZhgdWarnMapper zhgdWarnMapper;

    /**
     * 查询预警
     * 
     * @param id 预警主键
     * @return 预警
     */
    @Override
    public ZhgdWarn selectZhgdWarnById(Long id)
    {
        return zhgdWarnMapper.selectZhgdWarnById(id);
    }

    /**
     * 查询预警列表
     * 
     * @param zhgdWarn 预警
     * @return 预警
     */
    @Override
    public List<ZhgdWarn> selectZhgdWarnList(ZhgdWarn zhgdWarn)
    {
        return zhgdWarnMapper.selectZhgdWarnList(zhgdWarn);
    }

    /**
     * 新增预警
     * 
     * @param zhgdWarn 预警
     * @return 结果
     */
    @Override
    public int insertZhgdWarn(ZhgdWarn zhgdWarn)
    {
        zhgdWarn.setCreateTime(DateUtils.getNowDate());
        return zhgdWarnMapper.insertZhgdWarn(zhgdWarn);
    }

    /**
     * 修改预警
     * 
     * @param zhgdWarn 预警
     * @return 结果
     */
    @Override
    public int updateZhgdWarn(ZhgdWarn zhgdWarn)
    {
        zhgdWarn.setUpdateTime(DateUtils.getNowDate());
        return zhgdWarnMapper.updateZhgdWarn(zhgdWarn);
    }

    /**
     * 批量删除预警
     * 
     * @param ids 需要删除的预警主键
     * @return 结果
     */
    @Override
    public int deleteZhgdWarnByIds(Long[] ids)
    {
        return zhgdWarnMapper.deleteZhgdWarnByIds(ids);
    }

    /**
     * 删除预警信息
     * 
     * @param id 预警主键
     * @return 结果
     */
    @Override
    public int deleteZhgdWarnById(Long id)
    {
        return zhgdWarnMapper.deleteZhgdWarnById(id);
    }
}
