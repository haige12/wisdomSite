package cn.com.taiji.zhgd.mapper;

import java.util.List;
import cn.com.taiji.zhgd.domain.ZhgdCrane;

/**
 * 塔吊管理Mapper接口
 * 
 * @author haige
 * @date 2022-03-22
 */
public interface ZhgdCraneMapper 
{
    /**
     * 查询塔吊管理
     * 
     * @param id 塔吊管理主键
     * @return 塔吊管理
     */
    public ZhgdCrane selectZhgdCraneById(Long id);

    /**
     * 查询塔吊管理列表
     * 
     * @param zhgdCrane 塔吊管理
     * @return 塔吊管理集合
     */
    public List<ZhgdCrane> selectZhgdCraneList(ZhgdCrane zhgdCrane);

    /**
     * 新增塔吊管理
     * 
     * @param zhgdCrane 塔吊管理
     * @return 结果
     */
    public int insertZhgdCrane(ZhgdCrane zhgdCrane);

    /**
     * 修改塔吊管理
     * 
     * @param zhgdCrane 塔吊管理
     * @return 结果
     */
    public int updateZhgdCrane(ZhgdCrane zhgdCrane);

    /**
     * 删除塔吊管理
     * 
     * @param id 塔吊管理主键
     * @return 结果
     */
    public int deleteZhgdCraneById(Long id);

    /**
     * 批量删除塔吊管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhgdCraneByIds(Long[] ids);
}
