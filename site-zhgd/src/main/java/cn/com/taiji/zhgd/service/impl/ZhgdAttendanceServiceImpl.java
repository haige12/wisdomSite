package cn.com.taiji.zhgd.service.impl;

import java.util.List;

import cn.com.taiji.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.taiji.zhgd.mapper.ZhgdAttendanceMapper;
import cn.com.taiji.zhgd.domain.ZhgdAttendance;
import cn.com.taiji.zhgd.service.IZhgdAttendanceService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 考勤管理Service业务层处理
 * 
 * @author haige
 * @date 2022-03-30
 */
@Service
public class ZhgdAttendanceServiceImpl implements IZhgdAttendanceService 
{
    @Autowired
    private ZhgdAttendanceMapper zhgdAttendanceMapper;

    /**
     * 查询考勤管理
     * 
     * @param id 考勤管理主键
     * @return 考勤管理
     */
    @Override
    public ZhgdAttendance selectZhgdAttendanceById(Long id)
    {
        return zhgdAttendanceMapper.selectZhgdAttendanceById(id);
    }

    /**
     * 查询考勤管理列表
     * 
     * @param zhgdAttendance 考勤管理
     * @return 考勤管理
     */
    @Override
    public List<ZhgdAttendance> selectZhgdAttendanceList(ZhgdAttendance zhgdAttendance)
    {
        return zhgdAttendanceMapper.selectZhgdAttendanceList(zhgdAttendance);
    }

    /**
     * 新增考勤管理
     * 
     * @param zhgdAttendance 考勤管理
     * @return 结果
     */
    @Override
    public int insertZhgdAttendance(ZhgdAttendance zhgdAttendance)
    {
        zhgdAttendance.setCreateTime(DateUtils.getNowDate());
        return zhgdAttendanceMapper.insertZhgdAttendance(zhgdAttendance);
    }

    /**
     * 修改考勤管理
     * 
     * @param zhgdAttendance 考勤管理
     * @return 结果
     */
    @Override
    public int updateZhgdAttendance(ZhgdAttendance zhgdAttendance)
    {
        zhgdAttendance.setUpdateTime(DateUtils.getNowDate());
        return zhgdAttendanceMapper.updateZhgdAttendance(zhgdAttendance);
    }

    /**
     * 批量删除考勤管理
     * 
     * @param ids 需要删除的考勤管理主键
     * @return 结果
     */
    @Override
    public int deleteZhgdAttendanceByIds(Long[] ids)
    {
        return zhgdAttendanceMapper.deleteZhgdAttendanceByIds(ids);
    }

    /**
     * 删除考勤管理信息
     * 
     * @param id 考勤管理主键
     * @return 结果
     */
    @Override
    public int deleteZhgdAttendanceById(Long id)
    {
        return zhgdAttendanceMapper.deleteZhgdAttendanceById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int batchAdd(List<ZhgdAttendance> zhgdAttendanceList) {
        return zhgdAttendanceMapper.batchSave(zhgdAttendanceList);
    }
}
