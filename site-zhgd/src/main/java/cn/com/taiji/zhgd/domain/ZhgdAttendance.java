package cn.com.taiji.zhgd.domain;

import cn.com.taiji.common.annotation.Excel;
import cn.com.taiji.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotNull;

/**
 * 考勤管理对象 zhgd_attendance
 * 
 * @author haige
 * @date 2022-03-30
 */
public class ZhgdAttendance extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    @NotNull(message = "用户id不能为空")
    private Long id;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    @NotNull(message = "用户昵称不能为空")
    private String nickname;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private String company;

    /** 进园时间 */
    @Excel(name = "进园时间")
    private String inParkTime;

    /** 出园时间 */
    @Excel(name = "出园时间")
    private String outParkTime;

    /** 账户启用状态（0正常 1停用） */
    @Excel(name = "账户启用状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setCompany(String company) 
    {
        this.company = company;
    }

    public String getCompany() 
    {
        return company;
    }
    public void setInParkTime(String inParkTime) 
    {
        this.inParkTime = inParkTime;
    }

    public String getInParkTime() 
    {
        return inParkTime;
    }
    public void setOutParkTime(String outParkTime) 
    {
        this.outParkTime = outParkTime;
    }

    public String getOutParkTime() 
    {
        return outParkTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("nickname", getNickname())
            .append("company", getCompany())
            .append("inParkTime", getInParkTime())
            .append("outParkTime", getOutParkTime())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
