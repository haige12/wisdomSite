package cn.com.taiji.zhgd.service.impl;

import java.util.List;

import cn.com.taiji.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.taiji.zhgd.mapper.ZhgdDustMapper;
import cn.com.taiji.zhgd.domain.ZhgdDust;
import cn.com.taiji.zhgd.service.IZhgdDustService;

/**
 * 扬尘Service业务层处理
 * 
 * @author haige
 * @date 2022-03-22
 */
@Service
public class ZhgdDustServiceImpl implements IZhgdDustService 
{
    @Autowired
    private ZhgdDustMapper zhgdDustMapper;

    /**
     * 查询扬尘
     * 
     * @param id 扬尘主键
     * @return 扬尘
     */
    @Override
    public ZhgdDust selectZhgdDustById(Long id)
    {
        return zhgdDustMapper.selectZhgdDustById(id);
    }

    /**
     * 查询扬尘列表
     * 
     * @param zhgdDust 扬尘
     * @return 扬尘
     */
    @Override
    public List<ZhgdDust> selectZhgdDustList(ZhgdDust zhgdDust)
    {
        return zhgdDustMapper.selectZhgdDustList(zhgdDust);
    }

    /**
     * 新增扬尘
     * 
     * @param zhgdDust 扬尘
     * @return 结果
     */
    @Override
    public int insertZhgdDust(ZhgdDust zhgdDust)
    {
        zhgdDust.setCreateTime(DateUtils.getNowDate());
        return zhgdDustMapper.insertZhgdDust(zhgdDust);
    }

    /**
     * 修改扬尘
     * 
     * @param zhgdDust 扬尘
     * @return 结果
     */
    @Override
    public int updateZhgdDust(ZhgdDust zhgdDust)
    {
        zhgdDust.setUpdateTime(DateUtils.getNowDate());
        return zhgdDustMapper.updateZhgdDust(zhgdDust);
    }

    /**
     * 批量删除扬尘
     * 
     * @param ids 需要删除的扬尘主键
     * @return 结果
     */
    @Override
    public int deleteZhgdDustByIds(Long[] ids)
    {
        return zhgdDustMapper.deleteZhgdDustByIds(ids);
    }

    /**
     * 删除扬尘信息
     * 
     * @param id 扬尘主键
     * @return 结果
     */
    @Override
    public int deleteZhgdDustById(Long id)
    {
        return zhgdDustMapper.deleteZhgdDustById(id);
    }
}
