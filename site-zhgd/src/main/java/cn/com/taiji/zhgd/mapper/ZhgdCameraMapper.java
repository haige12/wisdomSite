package cn.com.taiji.zhgd.mapper;


import cn.com.taiji.zhgd.domain.ZhgdCamera;

import java.util.List;

/**
 * 摄像头Mapper接口
 * 
 * @author haige
 * @date 2022-03-22
 */
public interface ZhgdCameraMapper 
{
    /**
     * 查询摄像头
     * 
     * @param id 摄像头主键
     * @return 摄像头
     */
    public ZhgdCamera selectZhgdCameraById(Long id);

    /**
     * 查询摄像头列表
     * 
     * @param zhgdCamera 摄像头
     * @return 摄像头集合
     */
    public List<ZhgdCamera> selectZhgdCameraList(ZhgdCamera zhgdCamera);

    /**
     * 新增摄像头
     * 
     * @param zhgdCamera 摄像头
     * @return 结果
     */
    public int insertZhgdCamera(ZhgdCamera zhgdCamera);

    /**
     * 修改摄像头
     * 
     * @param zhgdCamera 摄像头
     * @return 结果
     */
    public int updateZhgdCamera(ZhgdCamera zhgdCamera);

    /**
     * 删除摄像头
     * 
     * @param id 摄像头主键
     * @return 结果
     */
    public int deleteZhgdCameraById(Long id);

    /**
     * 批量删除摄像头
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteZhgdCameraByIds(Long[] ids);
}
